package favoritesongmap;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jose
 */
public class FavoriteSongMap {

    public static void main(String[] args) {
        //My favorite song's info
        
        //Storing the song's info into 2 arrays
        String [] songKeys = {"songName","songAuthor","songPerformer","songYearRecorded",
            "songYearReleased","songGenre","songLength","songAvailableOnSpotify"};
        String [] songValues = {"You Know My Name","Chris Cornell","Chris Cornell","2006",
            "2006","Alternative rock","4.02 minutes","T"};
        
        //Storing both arrays' info in a map
        Map<String,String> songInfo = new HashMap<>();
        for(int i = 0; i < songKeys.length; i++)
        {
            songInfo.put(songKeys[i],songValues[i]);
        }
        
        //printing map's content
        System.out.println("Printing song's info:");
        System.out.println();
        //I had to figure out this one. Thanks :D
        songInfo.forEach(
                (key,value) -> System.out.println(key + ": " + value)
        );
    } 
}
